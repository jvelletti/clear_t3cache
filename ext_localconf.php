<?php
if (!defined ('TYPO3_MODE')) 	die ('Access denied.');

if (TYPO3_MODE == 'BE') {
	## Setting up script that can be run through cli_dispatch.phpsh
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['cliKeys']['clear_t3cache'] = array('EXT:clear_t3cache/cli/class.tx_clear_cache_cli.php', '_CLI_lowlevel');

}

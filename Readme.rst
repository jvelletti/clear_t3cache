STEPS
=====

1. Install the extension using extension_manager
2. Create a user backend with the following name: "_cli_lowlevel" with a random password WITHOUT ANY additional privileges.
3. To clear the cache using the command line in the root typo3 directory run the following:

*NIX:
php typo3/cli_dispatch.phpsh clear_t3cache

WINDOWS:
php typo3\\cli_dispatch.phpsh clear_t3cache

4. Wait for the message CLEAR CACHE!

Special:
========
5. if you want to clear just the Pagecache for on or more specif PageIds from cache, add the ids comma separated
php typo3\cli_dispatch.phpsh clear_t3cache pages 12,15


Note: This does not clean user sessions.

https://bitbucket.org/francisco_lopez/clear_t3cache
